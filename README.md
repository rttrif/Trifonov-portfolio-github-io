# Data Science Portfolio


This portfolio is a compilation of notebooks which I created for data analysis or for exploration of machine learning algorithms. A separate category is for separate projects.

## 1. Data Wrangling
Проекты посвященные предварительной обработке данных для анализа 

### Project 1 - Immigration of Canada
[В проекте выполняется очистка данных отражающих иммиграционные потоки Канады за 2015.](https://github.com/rttrif/Trifonov.portfolio.github.io/tree/master/1.%20Data%20Wrangling/Project%201%20-%20Immigration%20of%20Canada)

#### Описание структуры и результатов проекта 

- Проект состоит из двух разделов  
    - Section 1. Data Understanding
    - Section 2. Data Preparation
    
- В разделе Data Understanding выполнено 
    - Первичное описание исходных данных 
    - Выявлено, что данные имеют не читаемые заголовки, которые решено пропустить 
    - Выполнен анализ типов переменных 
    - Выполненана оценка качества исходных данных 
    - Установлено, что в данных имеются пропуски и дублируюеще значаения 
- В разделе Data Preparation выполено
    - Переимнование переменных и приведение их к единой структуре 
    - Слияение листов 2 и 3 
    - Конвертация типов данных 
    - Замена пропущенных значений 
    - Удаление дублирующих значений 
    - Удаление столбца classification_type т.к. в обоих наборах даных он содержит лишь одно значение 

## 2. Data Visualizations and Storytelling
Проекты посвященные визуализации и извлечению знаний из данных

## 3. Exploratory Data Analysis
Проекты посвященные разведочному анализу данных 

## 4. Feature engineering
Проекты посвященные проектированию признаков для машинного обучения 

## 5. Machine Learning
Проекты посвященные машинному обучению 

## 6. Deep Learning
Проекты посвященные глубокому изучению
